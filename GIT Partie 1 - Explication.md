# GIT Partie 1 - Explication

## Quelle sont les deux façons d'initialiser un dépôt ? (1 point)

Faire un git init pour créer un dépôt local puis le push sur le dépôt distant ou un git clone pour récupérer un dépôt distant existant et le créer en local.

## À quoi sert une Pull Request / Merge Request ? (1 point)

Une pull request ou merge request sert à fusionner les modification de deux branches. Il s'agit d'un outil qui va mettre en évidence les conflits et permettre au dev de les résoudre avant du fusionner les branches.

Elle est particulièrement utile dans les travaux d'équipe ou chacun travail sur sa branche et permet de discuter des modification apporter au code avant de merge.

## Qu'est ce qu'une branche ? (1 point)

une branche est un pointeur sur un commit 

## Vous venez de modifier un fichier. Comment créer un commit ? (3 points)

il faut fait un git add pour passer les fichiers dans la staging area

puis un git commit (-m pour ajouter un message) pour passer les fichiers de la staging area au dépôt local

(optionnel) un git push pour mettre le commit sur le dépôt distant 

## Pourquoi est-il plus prudent d'utiliser `origin/master` plutôt que `master` pour se mettre à jour ? (1 point)

origin/master est la branche master du dépôt local, qui est plus à même de ne pas contenir d'erreur par rapport a la branche master de la working copy qui peut avoir été modifié par des commits

## A quoi servent les commandes `git status`, `git log` et `git reflog` ? Quand les utiliser ? (2 points)

git status sert à afficher état actuel du projet : la branche courante, les fichiers modifiés, les fichiers dans la staging area et les fichiers présent en local uniquement, les conflits, etc.

git log montre l'historique des commit, utile pour retrouver les id de commit

git reflog montre l'historique des commandes git utilisées, très utile pour retrouver l'ensemble des commande réalisé précédemment et corriger leur effets

## Quelles sont les conditions qui doivent être réunies pour que des conflits surviennent ? (2 points)

avoir un fichier dans deux branches séparées, présentant des modifications sur des morceaux de codes similaires et en voulant merge les deux branches

## Imaginez que la branche master du dépôt distant contient de nouveaux commit. Comment intégrer ces commit dans votre branche ? (3 points)

en faisant une pull request

en faisant un git fetch de la branche voulant être récupérée, puis en faisant un 'git rebase nouveauCommit maBranche' pour placer nos commit à la suite des nouveaux commit dans l'arborescence de la working copy puis en  faisant un git push --force pour réécrire l'historique de fichiers du dépôt local

